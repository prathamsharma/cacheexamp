* # Cache Practice

* Ruby version
2.4.1

* Ruby version
5.0.1

* Building a Rails 5 Application with Memcache
https://devcenter.heroku.com/articles/building-a-rails-3-application-with-memcache

* install gem dalli
https://github.com/petergoldstein/dalli
* path: config/environments/*.rd
- config.cache_store = :mem_cache_store 

* install memcache on server/system
https://www.keycdn.com/support/what-is-memcached

main advantages of memcache are,

    Memcahe is distributed memory system.
    Data store on server.
    Reduces frequent database access.
    Memcached runs on Unix, Linux, Windows and Mac OS X.
    Memcached can compensate for insufficient ACID properties and it never blocks.
    Memcached is cross-platform
    Cross-DBMS.
    Its Cheap and efficient.

* Cache with redis - https://www.sitepoint.com/rails-model-caching-redis/
Install gems
gem 'redis'
gem 'redis-namespace'
gem 'redis-rails'
gem 'redis-rack-cache'

* path: config/environments/*.rd
config.cache_store = :redis_store, 'redis://localhost:6379/0/cache', { expires_in: 90.minutes }