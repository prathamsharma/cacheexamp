module WelcomesHelper
    def fetch_users
        users = $redis.get("users")
        if users.blank?
            users = User.all.to_json
            $redis.set("users", users)
        end
        @users_red = JSON.load users
    end
end
