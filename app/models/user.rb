class User < ApplicationRecord

    # start Dalli n memcache
    after_save    :expire_contact_all_cache
    after_destroy :expire_contact_all_cache

    def self.all_cached
        Rails.cache.fetch('User.all') { all.to_a }
    end

    def expire_contact_all_cache
        Rails.cache.delete('User.all')
    end
    # end Dalli n memcache


    # # start redis as cache
    # def self.users_count
    #     Rails.cache.fetch([cache_key, __method__], expires_in: 30.minutes) do
    #         self.count
    #     end
    # end
    # # end redis as cache

end
