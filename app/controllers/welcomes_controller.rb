class WelcomesController < ApplicationController
  def index
    # @stats = Rails.cache.stats.first.last
    @users = User.all_cached
  end
end
